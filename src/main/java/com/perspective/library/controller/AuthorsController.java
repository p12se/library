package com.perspective.library.controller;

import com.perspective.library.dto.AuthorDTO;
import com.perspective.library.dto.BaseDTO;
import com.perspective.library.dto.BookDTO;
import com.perspective.library.service.AuthorService;
import com.perspective.library.service.BookService;
import com.perspective.library.service.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@Controller
public class AuthorsController {

    private final AuthorService service;
    private final BookService bookService;
    private String errors;

    @Autowired
    public AuthorsController(AuthorService service, BookService bookService) {
        this.bookService = bookService;
        this.service = service;
    }

    @RequestMapping(path = "/authors", method = RequestMethod.GET)
    public String authorList(ModelMap model) throws ServiceException {

        model.addAttribute("authors", service.getAll());
        return "author/authors";

    }

    @RequestMapping(path = "/authors/{id}", method = RequestMethod.GET)
    public String authorById(ModelMap model, @PathVariable("id") long id) throws ServiceException {

        model.addAttribute("author", service.getById(id));
        return "author/author";

    }

    @RequestMapping(path = "/authors/{id}/update", method = RequestMethod.GET)
    public String authorUpdateForm(ModelMap model, @PathVariable("id") long id) throws ServiceException {
        if (errors != null) {
            model.addAttribute("message", errors);
        } else {
            model.addAttribute("message", "");
        }
        model.addAttribute("author", service.getById(id));
        return "author/update-author";

    }

    @RequestMapping(path = "/authors/{id}/update", method = RequestMethod.POST)
    public String authorUpdate(@ModelAttribute("author")AuthorDTO author, @PathVariable("id") long id) throws ServiceException {
        String validation = author.validate();
        if (validation.equals(BaseDTO.SUCCESS)) {
            service.update(author.getId(), author.getFullName(), author.getYearOfBirth(), author.getBiography());
        } else {
            errors = validation;
            return "redirect:/authors/" + id + "update";
        }
        return "redirect:/authors/" + author.getId();
    }

    @RequestMapping(path = "/authors/{id}/books", method = RequestMethod.GET)
    public String authorBooks(ModelMap model, @PathVariable("id") long id) throws ServiceException {

        model.addAttribute("books", service.getById(id).getBooks());
        model.addAttribute("author", id);
        return "book/books-by-author";
    }

    @RequestMapping(path = "/authors/add", method = RequestMethod.GET)
    public String authorAddForm(ModelMap model) throws ServiceException {
        if (errors != null) {
            model.addAttribute("message", errors);
        } else {
            model.addAttribute("message", "");
        }
        return "author/add-author";

    }

    @RequestMapping(path = "/authors/add", method = RequestMethod.POST)
    public String authorAdd(@ModelAttribute("author")AuthorDTO author) throws ServiceException {
        String validation = author.validate();
        if (validation.equals(BaseDTO.SUCCESS)) {
            service.create(author.getFullName(), author.getYearOfBirth(), author.getBiography());
        } else {
            errors = validation;
            return "redirect:/authors/add";
        }
        return "redirect:/authors";
    }

    @RequestMapping(path = "/authors/{id}/attach", method = RequestMethod.GET)
    public String attachBookForm(ModelMap model, @PathVariable("id") long id) throws ServiceException {

        model.addAttribute("books", bookService.getBooks());
        model.addAttribute("author_id", id);
        return "book/attach-book";
    }

    @RequestMapping(path = "/authors/{id}/attach", method = RequestMethod.POST)
    public String attachBook(@ModelAttribute("select_book") BookDTO book, ModelMap model, @PathVariable("id") long id) throws ServiceException {
        service.attach(book.getId(), id);
        return "redirect:/authors/" + id + "/books";
    }

    @RequestMapping(path = "/authors/{id}/detach", method = RequestMethod.POST)
    public String detachBook(@ModelAttribute("book") BookDTO book, ModelMap model, @PathVariable("id") long id) throws ServiceException {
        service.detach(book.getId(), id);
        return "redirect:/authors/" + id + "/books";
    }

}
