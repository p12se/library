package com.perspective.library.controller;

import com.perspective.library.dto.BaseDTO;
import com.perspective.library.dto.BookDTO;
import com.perspective.library.service.AuthorService;
import com.perspective.library.service.BookService;
import com.perspective.library.service.GenreService;
import com.perspective.library.service.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class BooksController {

    private final BookService service;
    private final GenreService genreService;
    private final AuthorService authorService;

    private String errors;

    @Autowired
    public BooksController(BookService bookService, GenreService genreService, AuthorService authorService) {
        this.service = bookService;
        this.genreService = genreService;
        this.authorService = authorService;
        this.errors = "";
    }

    @RequestMapping(path = "/books", method = RequestMethod.GET)
    public String bookList(ModelMap model) throws ServiceException {

        model.addAttribute("books", service.getBooks());
        return "book/books";
    }

    @RequestMapping(path = "/books/add", method = RequestMethod.GET)
    public String bookAddForm(ModelMap model) throws ServiceException {
        if (errors != null) {
            model.addAttribute("message", errors);
        }
        model.addAttribute("genres", genreService.getAll());
        model.addAttribute("authors", authorService.getAll());
        return "book/add-book";
    }

    @RequestMapping(path = "/books/add", method = RequestMethod.POST)
    public String bookAdd(@ModelAttribute("book") BookDTO book) throws ServiceException {
        String validation = book.validate();
        if (validation.equals(BaseDTO.SUCCESS)) {
            errors = null;
            service.add(book);
        } else {
            errors = validation;
            return "redirect:/books/add";
        }
        return "redirect:/books";
    }

    @RequestMapping(path = "/books/{id}/update", method = RequestMethod.GET)
    public String bookUpdateForm(ModelMap model, @PathVariable("id") long id) throws ServiceException {
        if (errors != null) {
            model.addAttribute("message", errors);
        } else {
            model.addAttribute("message", "");
        }
        model.addAttribute("book", service.getBookById(id));
        model.addAttribute("genres", genreService.getAll());
        model.addAttribute("authors", authorService.getAll());
        return "book/update-book";
    }

    @RequestMapping(path = "/books/{id}/update", method = RequestMethod.POST)
    public String bookUpdate(@PathVariable("id") long id, @ModelAttribute("book") BookDTO book) throws ServiceException {
        String validation = book.validate();
        if (validation.equals(BaseDTO.SUCCESS)) {
            errors = null;
            service.update(book);
        } else {
            errors = validation;
            return "redirect:/books/" + id + "/update";
        }
        return "redirect:/books";
    }
}
