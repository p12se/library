package com.perspective.library.controller;

import com.perspective.library.entities.Genre;
import com.perspective.library.service.GenreService;
import com.perspective.library.service.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class GenresController {

    private final GenreService genreService;

    @Autowired
    public GenresController(GenreService genreService) {
        this.genreService = genreService;
    }

    @RequestMapping(path = "/genres", method = RequestMethod.GET)
    public String genreList(ModelMap model) throws ServiceException {

        model.addAttribute("genres", genreService.getAll());
        return "genre/genres";

    }

    @RequestMapping(path = "/genres/add", method = RequestMethod.GET)
    public String genreAddForm(ModelMap model) throws ServiceException {

        return "genre/add-genre";

    }

    @RequestMapping(path = "/genres/add", method = RequestMethod.POST)
    public String genreAdd(@ModelAttribute("title")Genre genre, ModelMap model) throws ServiceException {
        if (genre.validate()) {
            genreService.add(genre.getTitle());
        } else {
            return "redirect:/genres/add";
        }
        return "redirect:/genres";

    }

    @RequestMapping(path = "/genres/{id}/update", method = RequestMethod.GET)
    public String genreUpdateForm(ModelMap model, @PathVariable("id") long id) throws ServiceException {

        model.addAttribute("genre", genreService.getById(id));
        return "genre/update-genre";

    }

    @RequestMapping(path = "/genres/{id}/update", method = RequestMethod.POST)
    public String genreUpdate(@ModelAttribute("genre") Genre genre, ModelMap model, @PathVariable("id") long id) throws ServiceException {
        if (genre.validate()) {
            genreService.update(genre.getId(), genre.getTitle());
        } else {
            return "redirect:/genres/" + id + "/update";
        }
        return "redirect:/genres";

    }
}
