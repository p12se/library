package com.perspective.library.dao;


import com.perspective.library.dao.exception.DAOException;
import com.perspective.library.entities.Author;

import java.util.Set;

public interface AuthorDAO extends DAO<Author> {

    Set<Author> getAuthorsByIdArray(Long[] ids) throws DAOException;
    void attachToAuthor(Long bookId, Long authorId) throws DAOException;
    void detachFromAuthor(Long bookId, Long authorId) throws DAOException;

}
