package com.perspective.library.dao;


import com.perspective.library.entities.Book;

public interface BookDAO extends DAO<Book> {

}
