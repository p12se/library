package com.perspective.library.dao;


import com.perspective.library.dao.exception.DAOException;
import com.perspective.library.entities.Entity;

import java.util.Set;

public interface DAO<T extends Entity> {
    T getById(Long id) throws DAOException;
    Set<T> getAll() throws DAOException;
    void add(T entity) throws DAOException;
    void update(T entity) throws DAOException;
}
