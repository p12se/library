package com.perspective.library.dao;

import com.perspective.library.entities.Genre;

public interface GenreDAO extends DAO<Genre> {
}
