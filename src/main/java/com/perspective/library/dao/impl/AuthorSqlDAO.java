package com.perspective.library.dao.impl;

import com.perspective.library.dao.AuthorDAO;
import com.perspective.library.dao.exception.DAOException;
import com.perspective.library.entities.Author;
import com.perspective.library.entities.Book;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

@Repository
@Transactional
public class AuthorSqlDAO extends BaseDAO<Author> implements AuthorDAO {

    public AuthorSqlDAO() {
        super(Author.class);
    }

    @Override
    public Set<Author> getAuthorsByIdArray(Long[] ids) throws DAOException {
        Set authors;
        Session session = null;
        try {
            session = sessionFactory.openSession();
            Query query = session.createQuery("FROM Author a WHERE a.id IN (:ids)");
            query.setParameterList("ids", Arrays.asList(ids));
            authors = new HashSet(query.list());
        } catch (Exception e) {
            throw new DAOException("Can't get authors by ids=" + Arrays.asList(ids), e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return authors;
    }

    @Override
    public void attachToAuthor(Long bookId, Long authorId) throws DAOException {
        Session session = null;
        try {
            session = sessionFactory.openSession();
            Author author = (Author) session.get(Author.class, authorId);
            Book book = (Book) session.get(Book.class, bookId);
            Set<Author> authors = book.getAuthors();
            authors.add(author);
            session.beginTransaction();
            session.update(author);
            session.update(book);
            session.getTransaction().commit();
        } catch (Exception e) {
            throw new DAOException("Can't attach book id=" + bookId + " to author id=" + authorId, e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    @Override
    public void detachFromAuthor(Long bookId, Long authorId) throws DAOException {
        Session session = null;
        try {
            session = sessionFactory.openSession();
            Author author = (Author) session.get(Author.class, authorId);
            Book book = (Book) session.get(Book.class, bookId);
            Set<Author> authors = book.getAuthors();
            authors.remove(author);
            session.beginTransaction();
            session.update(book);
            session.getTransaction().commit();
        } catch (Exception e) {
            throw new DAOException("Can't detach book id=" + bookId + " from author id=" + authorId, e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

}
