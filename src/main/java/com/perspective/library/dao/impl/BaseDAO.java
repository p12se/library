package com.perspective.library.dao.impl;

import com.perspective.library.dao.DAO;
import com.perspective.library.dao.exception.DAOException;
import com.perspective.library.entities.Entity;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Set;

@Repository
@Transactional
public class BaseDAO<T extends Entity> implements DAO<T> {

    private Class<T> entityType;
    @Autowired
    protected SessionFactory sessionFactory;

    public BaseDAO(Class<T> entityType){
        this.entityType = entityType;
    }

    @Override
    public T getById(Long id) throws DAOException {
        Session session = null;
        try {
            session = sessionFactory.openSession();
            return  (T) session.get(entityType, id);
        } catch (Exception e) {
            throw new DAOException("Can't get author with id=" + id, e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    @Override
    public Set<T> getAll() throws DAOException {
        Set<T> entities;
        Session session = null;
        try {
            session = sessionFactory.openSession();
            entities = new HashSet<T>(session.createCriteria(entityType).list());
        } catch (Exception e) {
            throw new DAOException("Can't get authors", e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return entities;
    }

    @Transactional
    @Override
    public void add(T entity) throws DAOException {
        Session session = null;
        try {
            session = sessionFactory.openSession();
            session.beginTransaction();
            session.save(entity);
            session.getTransaction().commit();
        } catch (Exception e) {
            throw new DAOException("Can't add the entity class: " + entityType , e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    @Transactional
    @Override
    public void update(T entity) throws DAOException {
        Session session = null;
        try {
            session = sessionFactory.openSession();
            session.beginTransaction();
            session.update(entity);
            session.getTransaction().commit();
        } catch (Exception e) {
            throw new DAOException("Can't add the entity class: " + entityType, e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }
}
