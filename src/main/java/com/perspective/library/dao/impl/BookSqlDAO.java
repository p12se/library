package com.perspective.library.dao.impl;

import com.perspective.library.dao.BookDAO;
import com.perspective.library.entities.Book;
import org.springframework.stereotype.Repository;

@Repository
public class BookSqlDAO extends BaseDAO<Book> implements BookDAO {

    public BookSqlDAO() {
        super(Book.class);
    }


}
