package com.perspective.library.dao.impl;

import com.perspective.library.dao.GenreDAO;
import com.perspective.library.entities.Genre;

public class GenreSqlDAO extends BaseDAO<Genre> implements GenreDAO {

    public GenreSqlDAO() {
        super(Genre.class);
    }
}
