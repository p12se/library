package com.perspective.library.dto;


import java.time.LocalDate;

public class AuthorDTO extends BaseDTO {

    private String fullName;
    private int yearOfBirth;
    private String biography;

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public int getYearOfBirth() {
        return yearOfBirth;
    }

    public void setYearOfBirth(int yearOfBirth) {
        this.yearOfBirth = yearOfBirth;
    }

    public String getBiography() {
        return biography;
    }

    public void setBiography(String biography) {
        this.biography = biography;
    }

    public String validate() {
        boolean fullNameExists = getFullName() != null && !getFullName().equals("");
        boolean primaryYear = getYearOfBirth() < LocalDate.now().getYear();
        boolean biographyExists = getBiography() != null && !getBiography().equals("");
        if (!fullNameExists) {
            return properties.getProperty("author.invalidName");
        }
        if (!primaryYear) {
            return properties.getProperty("author.invalidYear");
        }
        if (!biographyExists) {
            return properties.getProperty("author.biographyNotExists");
        }
        return SUCCESS;
    }
}
