package com.perspective.library.dto;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class BaseDTO {

    public static final String SUCCESS = "success";

    private long id;
    private FileInputStream fileInputStream;
    Properties properties = new Properties();

    BaseDTO() {
        try {
            fileInputStream = new FileInputStream("/home/andrew/vp/Library/src/main/resources/errors.properties");
            properties.load(fileInputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
