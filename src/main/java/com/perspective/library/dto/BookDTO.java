package com.perspective.library.dto;

import java.time.LocalDate;

public class BookDTO extends BaseDTO {

    private String title;
    private Long genreId;
    private int yearOfPublishing;
    private Long[] authorsId;

    public BookDTO() {
        super();
    }

    public String getTitle() {
        return title;
    }

    public String validate() {
        boolean authorExists = getAuthorsId() != null && getAuthorsId().length != 0;
        boolean primaryYear = getYearOfPublishing() <= LocalDate.now().getYear();
        boolean titleExists = getTitle() != null && !getTitle().equals("");
        boolean genreExists = getGenreId() != 0 && getGenreId() != null;
        if (!authorExists) {
            return properties.getProperty("book.authorNotExists");
        }
        if (!primaryYear) {
            return properties.getProperty("book.invalidYear");
        }
        if (!titleExists) {
            return properties.getProperty("book.titleNotExists");
        }
        if (!genreExists) {
            return properties.getProperty("book.invalidGenre");
        }
        return SUCCESS;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getGenreId() {
        return genreId;
    }

    public void setGenreId(Long genreId) {
        this.genreId = genreId;
    }

    public int getYearOfPublishing() {
        return yearOfPublishing;
    }

    public void setYearOfPublishing(int yearOfPublishing) {
        this.yearOfPublishing = yearOfPublishing;
    }

    public Long[] getAuthorsId() {
        return authorsId;
    }

    public void setAuthorsId(Long[] authorsId) {
        this.authorsId = authorsId;
    }
}
