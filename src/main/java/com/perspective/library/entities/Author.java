package com.perspective.library.entities;


import javax.persistence.*;
import java.util.Set;

@javax.persistence.Entity
@Table(name = "author")
public class Author extends Entity {

    @Column(name="fullname")
    private String fullName;

    @Column(name = "year_of_birth")
    private int yearOfBirth;

    @Column(name = "biography")
    private String biography;

    @ManyToMany(fetch = FetchType.LAZY, targetEntity = Book.class)
    @JoinTable(name="author_book",
            joinColumns = @JoinColumn(name="author_id"),
            inverseJoinColumns = @JoinColumn(name="book_id")
    )
    private Set<Book> books;

    public Author(String fullName, int yearOfBirth, String biography) {
        this.fullName = fullName;
        this.yearOfBirth = yearOfBirth;
        this.biography = biography;
    }

    public Author() {}


    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public int getYearOfBirth() {
        return yearOfBirth;
    }

    public void setYearOfBirth(int yearOfBirth) {
        this.yearOfBirth = yearOfBirth;
    }

    public String getBiography() {
        return biography;
    }

    public void setBiography(String biography) {
        this.biography = biography;
    }

    public Set<Book> getBooks() {
        return books;
    }

    public void setBooks(Set<Book> books) {
        this.books = books;
    }

    @Override
    public String toString() {
        return "Author";
    }
}
