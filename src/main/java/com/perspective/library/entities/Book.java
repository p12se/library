package com.perspective.library.entities;


import javax.persistence.*;
import java.util.Set;

@javax.persistence.Entity
@Table(name = "book")
public class Book extends Entity{

    @Column(name = "title")
    private String title;
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = "genre")
    private Genre genre;
    @Column(name = "year_of_publishing")
    private int yearOfPublishing;
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name="author_book",
            inverseJoinColumns = @JoinColumn(name="author_id"),
            joinColumns = @JoinColumn(name="book_id")
    )
    private Set<Author> authors;

    public Book(long id, String title, Genre genre, int yearOfPublishing, Set<Author> authors) {
        this.id = id;
        this.title = title;
        this.genre = genre;
        this.yearOfPublishing = yearOfPublishing;
        this.authors = authors;
    }

    public Book(long id, String title, Genre genre, int yearOfPublishing) {
        this.id = id;
        this.title = title;
        this.genre = genre;
        this.yearOfPublishing = yearOfPublishing;
    }

    public Book(String title, Genre genre, int yearOfPublishing) {
        this.title = title;
        this.genre = genre;
        this.yearOfPublishing = yearOfPublishing;
    }

    public Book() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    public int getYearOfPublishing() {
        return yearOfPublishing;
    }

    public void setYearOfPublishing(int yearOfPublishing) {
        this.yearOfPublishing = yearOfPublishing;
    }

    public Set<Author> getAuthors() {
        return authors;
    }

    public void setAuthors(Set<Author> authors) {
        this.authors = authors;
    }

}
