package com.perspective.library.entities;

import javax.persistence.Column;
import javax.persistence.Table;

@javax.persistence.Entity
@Table(name = "genre")
public class Genre extends Entity {
    
    private String title;

    public Genre(String title) {
        this.title = title;
    }

    public Genre(long id, String title) {

        this.id = id;
        this.title = title;
    }

    public Genre(long id) {
        this.id = id;
    }

    public Genre() {}

    @Column(name = "title")
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean validate() {
        return getTitle() != null && !getTitle().isEmpty();
    }
}
