package com.perspective.library.service;

import com.perspective.library.dao.AuthorDAO;
import com.perspective.library.dao.exception.DAOException;
import com.perspective.library.dao.impl.AuthorSqlDAO;
import com.perspective.library.entities.Author;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

@Component
public class AuthorService {

    @Autowired
    private AuthorDAO dao;

    public AuthorService() {
        this.dao = new AuthorSqlDAO();
    }

    @Transactional
    public void create(String fullName, int yearOfBirth, String biography) throws ServiceException {
        Author author = new Author(fullName, yearOfBirth, biography);
        try {
            dao.add(author);
        } catch (DAOException e) {
            throw new ServiceException("Can't create a new author", e);
        }
    }

    public Author getById(long id) throws ServiceException {
        try {
            return dao.getById(id);
        } catch (DAOException e) {
            throw new ServiceException("Can't get author", e);
        }
    }

    public Set<Author> getAll() throws ServiceException {
        try {
            return dao.getAll();
        } catch (DAOException e) {
            throw new ServiceException("Can't get authors", e);
        }
    }

    @Transactional
    public void update(long id, String fullName, int yearOfBirth, String biography) throws ServiceException {
        try {
            Author author = dao.getById(id);
            if (author != null) {
                author.setFullName(fullName);
                author.setYearOfBirth(yearOfBirth);
                author.setBiography(biography);
                dao.update(author);
            } else {
                throw new ServiceException("Author not found");
            }
        } catch (DAOException e) {
            throw new ServiceException("Can't update author", e);
        }
    }

    public void attach(long bookId, long authorId) throws ServiceException {
        try {
            dao.attachToAuthor(bookId, authorId);
        } catch (DAOException e) {
            throw new ServiceException("Can't attach book id=" + bookId + " to author id=" + authorId, e);
        }
    }

    public void detach(long bookId, long authorId) throws ServiceException {
        try {
            dao.detachFromAuthor(bookId, authorId);
        } catch (DAOException e) {
            throw new ServiceException("Can't detach book id=" + bookId + " from author id=" + authorId, e);
        }
    }
}
