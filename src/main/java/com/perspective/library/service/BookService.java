package com.perspective.library.service;

import com.perspective.library.dao.AuthorDAO;
import com.perspective.library.dao.BookDAO;
import com.perspective.library.dao.GenreDAO;
import com.perspective.library.dao.exception.DAOException;
import com.perspective.library.dao.impl.AuthorSqlDAO;
import com.perspective.library.dao.impl.BookSqlDAO;
import com.perspective.library.dao.impl.GenreSqlDAO;
import com.perspective.library.dto.BookDTO;
import com.perspective.library.entities.Author;
import com.perspective.library.entities.Book;
import com.perspective.library.entities.Genre;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component
public class BookService {

    @Autowired
    private BookDAO bookSqlDAO;
    @Autowired
    private GenreDAO genreSqlDAO;
    @Autowired
    private AuthorDAO authorSqlDAO;

    public BookService() {
        bookSqlDAO = new BookSqlDAO();
        genreSqlDAO = new GenreSqlDAO();
        authorSqlDAO = new AuthorSqlDAO();
    }


    public Set<Book> getBooks() throws ServiceException {
        Set<Book> books;
        try {
            books = bookSqlDAO.getAll();
        } catch (DAOException e) {
            throw new ServiceException("Can't get list of books", e);
        }
        return books;
    }

    public Book getBookById(long id) throws ServiceException{
        try {
            return bookSqlDAO.getById(id);
        } catch (DAOException e) {
            throw new ServiceException("Can't get book with id=" + id, e);
        }
    }

    public void add(BookDTO bookDTO) throws ServiceException {
        try {
            Genre genre = genreSqlDAO.getById(bookDTO.getGenreId());
            Book book = new Book(bookDTO.getTitle(), genre, bookDTO.getYearOfPublishing());
            Set<Author> authors = authorSqlDAO.getAuthorsByIdArray(bookDTO.getAuthorsId());
            book.setAuthors(authors);
            bookSqlDAO.add(book);
        } catch (DAOException e) {
            throw new ServiceException("Can't add book", e);
        }
    }

    public void update(BookDTO bookDTO) throws ServiceException {
        try {
            Genre genre = genreSqlDAO.getById(bookDTO.getGenreId());
            Book book = new Book(bookDTO.getTitle(), genre, bookDTO.getYearOfPublishing());
            Set<Author> authors = authorSqlDAO.getAuthorsByIdArray(bookDTO.getAuthorsId());
            book.setAuthors(authors);
            book.setId(bookDTO.getId());
            bookSqlDAO.update(book);
        } catch (DAOException e) {
            throw new ServiceException("Can't update book", e);
        }
    }
}
