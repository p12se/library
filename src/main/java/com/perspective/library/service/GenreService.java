package com.perspective.library.service;


import com.perspective.library.dao.GenreDAO;
import com.perspective.library.dao.exception.DAOException;
import com.perspective.library.entities.Genre;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Set;

public class GenreService {

    @Autowired
    private GenreDAO genreSqlDAO;

    public Set<Genre> getAll() throws ServiceException {
        try {
            return genreSqlDAO.getAll();
        } catch (DAOException e) {
            throw new ServiceException("Can't get services", e);
        }
    }

    public Genre getById(long id) throws ServiceException {
        try {
            return genreSqlDAO.getById(id);
        } catch (DAOException e) {
            throw new ServiceException("Can't get genre with id=" + id, e);
        }
    }

    public void add(String title) throws ServiceException {
        try {
            genreSqlDAO.add(new Genre(title));
        } catch (DAOException e) {
            throw new ServiceException("Can't add new genre", e);
        }
    }

    public void update(long id, String title) throws ServiceException {
        try {
            genreSqlDAO.update(new Genre(id, title));
        } catch (DAOException e) {
            throw new ServiceException("Can't update genre with id=" + id, e);
        }
    }
}
