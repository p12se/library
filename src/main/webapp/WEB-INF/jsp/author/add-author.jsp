<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<c:import url="../head.jsp"/>
<body>
<div class="container">
    <c:import url="../header.jsp"/>
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <c:if test='${!message.equals("")}'>
                <p class="bg-danger">${message}</p>
            </c:if>
            <form class="form" role="form" method="post" action="${pageContext.request.contextPath}/authors/add">
                <div class="form-group">
                    <label class="sr-only" for="full_name">ФИО автора</label>
                    <input type="text" maxlength="250" name="fullName" class="form-control" id="full_name" placeholder="ФИО автора">
                </div>
                <div class="form-group">
                    <label class="sr-only" for="year_of_birth">Год рождения</label>
                    <input type="number" name="yearOfBirth" maxlength="4" class="form-control" id="year_of_birth"
                           placeholder="Год рождения">
                </div>
                <div class="form-group">
                    <label class="sr-only" for="biography">Биография</label>
                    <textarea class="form-control bio" placeholder="Биография" id="biography" name="biography"
                              rows="20"></textarea>
                </div>
                <button type="submit" class="btn btn-default">Добавить</button>
            </form>
        </div>
    </div>
</div>
</body>
</html>