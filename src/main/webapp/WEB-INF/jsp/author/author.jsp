<jsp:useBean id="author" scope="request" type="com.perspective.library.entities.Author"/>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<c:import url="../head.jsp"/>
<body>
<div class="container">
    <c:import url="../header.jsp"/>
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <h3>${author.fullName}</h3>
            <h5>Год рождения: ${author.yearOfBirth}</h5>
            <h4>Биография</h4>
            <p>${author.biography}</p>
        </div>
    </div>
</div>
</body>
</html>
