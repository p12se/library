<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<c:import url="../head.jsp"/>
<body>
<div class="container">
    <c:import url="../header.jsp"/>
    <div class="row" id="authors-table">
        <div class="col-md-8 col-md-offset-2">
            <table class="table table-striped table-hover">
                <thead>
                <tr>
                    <td>#</td>
                    <td>ФИО автора</td>
                </tr>
                </thead>
                <tbody>
                <jsp:useBean id="authors" scope="request" type="java.util.Set"/>
                <c:forEach var="author" items="${authors}">
                    <tr>
                        <td>${author.id}</td>
                        <td><a href="${pageContext.request.contextPath}/authors/${author.id}">${author.fullName}</a>
                        </td>
                        <td><a href="${pageContext.request.contextPath}/authors/${author.id}/update">Изменить</a></td>
                        <td>
                            <a href="${pageContext.request.contextPath}/authors/${author.id}/books">Посмотреть книги</a>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
                <tfoot>
                <tr>
                    <td></td>
                    <td>
                        <a href="${pageContext.request.contextPath}/authors/add" class="btn btn-primary" id="add-author">Добавить
                            автора</a>
                    </td>
                </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
</body>
</html>
