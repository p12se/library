<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<jsp:useBean id="author" scope="request" type="com.perspective.library.entities.Author"/>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<fmt:requestEncoding value="UTF-8" />
<!DOCTYPE html>
<html>
<c:import url="../head.jsp"/>
<body>
<div class="container">
    <c:import url="../header.jsp"/>
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <c:if test='${!message.equals("")}'>
                <p class="bg-danger">${message}</p>
            </c:if>
            <form class="form" role="form" accept-charset=«UTF-8 method="post" commandName="author" action="${pageContext.request.contextPath}/authors/${author.id}/update">
                <input type="text" name="id" id="id" value="${author.id}" style="display: none">
                <div class="form-group">
                    <label class="sr-only" for="fullname">ФИО автора</label>
                    <input type="text" name="fullName" maxlength="250" class="form-control" id="fullname" placeholder="ФИО автора"
                           value="${author.fullName}">
                </div>
                <div class="form-group">
                    <label class="sr-only" for="year_of_birth">Год рождения</label>
                    <input type="number" maxlength="4" name="yearOfBirth" class="form-control" id="year_of_birth"
                           placeholder="Год рождения" value="${author.yearOfBirth}">
                </div>
                <div class="form-group">
                    <label class="sr-only" for="biography">Биография</label>
                    <textarea class="form-control bio" placeholder="Биография" id="biography" name="biography"
                              rows="20">${author.biography}</textarea>
                </div>
                <button type="submit" class="btn btn-default">Изиенить</button>
            </form>
        </div>
    </div>
</div>
</body>
</html>
