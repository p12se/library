<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:useBean id="message" scope="request" type="java.lang.String"/>
<!DOCTYPE html>
<html>
<c:import url="../head.jsp"/>
<body>
<div class="container">
    <c:import url="../header.jsp"/>
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <c:if test='${!message.equals("")}'>
                <p class="bg-danger">${message}</p>
            </c:if>
            <form class="form" role="form" method="post" commandName="book" action="${pageContext.request.contextPath}/books/add">
                <div class="form-group">
                    <label class="sr-only" for="title">Название книги</label>
                    <input type="text" maxlength="250" name="title" class="form-control" id="title" placeholder="Название книги">
                </div>
                <div class="form-group">
                    <label class="sr-only" for="year_of_publishing">Год издания</label>
                    <input type="number" name="yearOfPublishing" maxlength="4" class="form-control" id="year_of_publishing"
                           placeholder="Год издания">
                </div>
                <div class="form-group">
                    <label class="sr-only" for="genre">Жанр</label>
                    <select id="genre" class="form-control" name="genreId">
                        <jsp:useBean id="genres" scope="request" type="java.util.Set"/>
                        <c:forEach var="genre" items="${genres}">
                            <option value="${genre.id}">${genre.title}</option>
                        </c:forEach>
                    </select>
                </div>
                <div class="form-group">
                    <label class="sr-only" for="authors">Жанр</label>
                    <select id="authors" class="form-control" multiple name="authorsId" size="10">
                        <jsp:useBean id="authors" scope="request" type="java.util.Set"/>
                        <c:forEach var="author" items="${authors}">
                            <option value="${author.id}">${author.fullName}</option>
                        </c:forEach>
                    </select>
                </div>
                <button type="submit" class="btn btn-default">Добавить</button>
            </form>
        </div>
    </div>
</div>
</body>
</html>
