<jsp:useBean id="author_id" scope="request" type="java.lang.Long"/>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<c:import url="../head.jsp"/>
<body>
<div class="container">
    <c:import url="../header.jsp"/>
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <form class="form" role="form" commandname="book" method="post" action="${pageContext.request.contextPath}/authors/${author_id}/attach">
                <div class="form-group">
                    <label class="sr-only" for="books">Жанр</label>
                    <select id="books" class="form-control" name="id" size="25">
                        <jsp:useBean id="books" scope="request" type="java.util.Set"/>
                        <c:forEach var="book" items="${books}">
                            <option value="${book.id}">${book.title}</option>
                        </c:forEach>
                    </select>
                </div>
                <button type="submit" class="btn btn-default">Привязать</button>
            </form>
        </div>
    </div>
</div>
</body>
</html>

