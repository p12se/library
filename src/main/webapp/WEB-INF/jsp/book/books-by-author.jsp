<jsp:useBean id="author" scope="request" type="java.lang.Long"/>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<c:import url="../head.jsp"/>
<body>
<div class="container">
    <c:import url="../header.jsp"/>
    <div class="row" id="authors-table">
        <div class="col-md-8 col-md-offset-2">
            <table class="table table-striped table-hover">
                <thead>
                <tr>
                    <td>#</td>
                    <td>Название</td>
                    <td>Год издания</td>
                    <td>Авторы</td>
                    <td>Жанр</td>
                </tr>
                </thead>
                <tbody>
                <jsp:useBean id="books" scope="request" type="java.util.Set"/>
                <c:forEach var="book" items="${books}">
                    <tr>
                        <td>${book.id}</td>
                        <td><a href="${pageContext.request.contextPath}/books/${book.id}/update">${book.title}</a></td>
                        <td>${book.yearOfPublishing}</td>
                        <td><c:forEach var="author" items="${book.authors}"><a href="${pageContext.request.contextPath}/authors/${author.id}">${author.fullName}</a></c:forEach></td>
                        <td>${book.genre.title}</td>
                        <td>
                            <form method="post" commandname="book" action="${pageContext.request.contextPath}/authors/${author}/detach">
                                <input style="display: none" value="${book.id}" name="id">
                                <button type="submit" class="btn btn-primary">Отвязать</button>
                            </form>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
                <tfoot>
                <tr>
                    <td></td>
                    <td>
                        <a href="${pageContext.request.contextPath}/authors/${author}/attach" class="btn btn-primary" id="attach-book">Привязать книгу</a>
                    </td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
</body>
</html>
