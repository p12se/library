<jsp:useBean id="book" scope="request" type="com.perspective.library.entities.Book"/>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:useBean id="message" scope="request" type="java.lang.String"/>
<!DOCTYPE html>
<html>
<c:import url="../head.jsp"/>
<body>
<div class="container">
    <c:import url="../header.jsp"/>
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <c:if test='${!message.equals("")}'>
                <p class="bg-danger">${message}</p>
            </c:if>
            <form class="form" role="form" method="post" action="${pageContext.request.contextPath}/books/${book.id}/update">
                <input id="id" name="id" value="${book.id}" style="display: none">
                <div class="form-group">
                    <label class="sr-only" for="title">Название книги</label>
                    <input type="text" maxlength="250" name="title" class="form-control" id="title" value="${book.title}"
                           placeholder="Название книги">
                </div>
                <div class="form-group">
                    <label class="sr-only" for="year_of_publishing">Год издания</label>
                    <input type="number" name="yearOfPublishing" maxlength="4" min="1000" class="form-control" value="${book.yearOfPublishing}"
                           id="year_of_publishing"
                           placeholder="Год издания">
                </div>
                <div class="form-group">
                    <label class="sr-only" for="genre">Жанр</label>
                    <select id="genre" class="form-control" name="genreId">
                        <jsp:useBean id="genres" scope="request" type="java.util.Set"/>
                        <c:forEach var="genre" items="${genres}">
                            <c:if test="${genre.id != book.genre.id}">
                                <option value="${genre.id}">${genre.title}</option>
                            </c:if>
                            <c:if test="${genre.id == book.genre.id}">
                                <option selected value="${genre.id}">${genre.title}</option>
                            </c:if>
                        </c:forEach>
                    </select>
                </div>
                <div class="form-group">
                    <label class="sr-only" for="authors">Жанр</label>
                    <select id="authors" class="form-control" multiple name="authorsId" size="10">
                        <jsp:useBean id="authors" scope="request" type="java.util.Set"/>
                        <c:forEach var="author" items="${authors}">

                            <option value="${author.id}"
                            <c:forEach var="bookAuthor" items="${book.authors}">

                                <c:if test="${bookAuthor.id == author.id}">
                                    selected
                                </c:if>
                            </c:forEach>
                            >${author.fullName}</option>

                        </c:forEach>
                    </select>
                </div>
                <button type="submit" class="btn btn-default">Сохранить</button>
            </form>
        </div>
    </div>
</div>
</body>
</html>
