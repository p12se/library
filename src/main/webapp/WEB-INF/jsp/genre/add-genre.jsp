<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<c:import url="../head.jsp"/>
<body>
<div class="container">
    <c:import url="../header.jsp"/>
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <form class="form" role="form" commandName="genre" method="post" action="${pageContext.request.contextPath}/genres/add">
                <div class="form-group">
                    <label class="sr-only" for="title">Название жанра</label>
                    <input type="text" maxlength="250" name="title" class="form-control" id="title" placeholder="Название жанра">
                </div>
                <button type="submit" class="btn btn-default">Добавить</button>
            </form>
        </div>
    </div>
</div>
</body>
</html>


