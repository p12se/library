<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<c:import url="../head.jsp"/>
<body>
<div class="container">
    <c:import url="../header.jsp"/>
    <div class="row" id="authors-table">
        <div class="col-md-8 col-md-offset-2">
            <table class="table table-striped table-hover">
                <thead>
                <tr>
                    <td>#</td>
                    <td>Название</td>
                </tr>
                </thead>
                <tbody>
                <jsp:useBean id="genres" scope="request" type="java.util.Set"/>
                <c:forEach var="genre" items="${genres}">
                    <tr>
                        <td>${genre.id}</td>
                        <td><a href="${pageContext.request.contextPath}/genres/${genre.id}/update">${genre.title}</a></td>
                    </tr>
                </c:forEach>
                </tbody>
                <tfoot>
                <tr>
                    <td></td>
                    <td>
                        <a href="${pageContext.request.contextPath}/genres/add" class="btn btn-primary" id="add-genre">Добавить жанр</a>
                    </td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
</body>
</html>

