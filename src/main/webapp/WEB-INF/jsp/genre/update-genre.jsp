<jsp:useBean id="genre" scope="request" type="com.perspective.library.entities.Genre"/>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<c:import url="../head.jsp"/>
<body>
<div class="container">
    <c:import url="../header.jsp"/>
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <form class="form" role="form" method="post" commandName="genre" action="${pageContext.request.contextPath}/genres/${genre.id}/update">
                <div class="form-group">
                    <label class="sr-only" for="title">Название жанра</label>
                    <input type="text" maxlength="250" name="title" class="form-control" id="title" placeholder="Название жанра" value="${genre.title}">
                    <input value="${genre.id}" name="id" style="display: none">
                </div>
                <button type="submit" class="btn btn-default">Изменить</button>
            </form>
        </div>
    </div>
</div>
</body>
</html>

