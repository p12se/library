<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<head>
    <title>Библиотека</title>
    <style>
        <%@include file="../../css/bootstrap.min.css"%>
    </style>
    <style>
        <%@include file="../../css/style.css"%>
    </style>
    <style>
        <%@include file="../../css/author.css"%>
    </style>

    <script>
        <%@include file="../../js/jquery-3.2.0.min.js"%>
    </script>
    <script>
        <%@include file="../../js/bootstrap.min.js"%>
    </script>
    <meta charset="utf-8">
</head>
