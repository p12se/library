<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="row nav-bar">
    <h3 class="title">Библиотека</h3>
    <ul class="links">
        <li class="link"><a href="${pageContext.request.contextPath}/authors">Авторы</a></li>
        <li class="link"><a href="${pageContext.request.contextPath}/genres">Жанры</a></li>
        <li class="link"><a href="${pageContext.request.contextPath}/books"> Книги</a></li>
    </ul>
</div>
